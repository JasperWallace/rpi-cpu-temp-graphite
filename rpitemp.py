#!/usr/bin/env python
#
#
#

import os.path, subprocess, sys

class Temp:
  def __init__(self):
    if os.path.exists("/sys/class/thermal/thermal_zone0/temp"):
      self.method = "tz"
    else:
      self.method = "measure_temp"
  def read(self):
    if self.method == "tz":
      fh = open("/sys/class/thermal/thermal_zone0/temp", "r")
      reading = float(fh.read().strip()) / 1000.0
      fh.close()
      return reading
    elif self.method == "measure_temp":
      # call sudo /opt/vc/bin/vcgencmd measure_temp
      # temp=39.0'C
      try:
        reading = subprocess.check_output(["sudo", "/opt/vc/bin/vcgencmd", "measure_temp"], stderr=subprocess.STDOUT)
        reading = float(reading.split('=')[1].split('\'')[0])
        return reading
      except Exception as err:
        print("getting temp failed with:")
        print(err)
        print("output: ", err.output)
        print("return code: ", err.returncode)
        print("this command needs sudo")
      return 0.0
    else:
      return 0.0

if __name__ == "__main__":
  t = Temp()
  print(t.read())

