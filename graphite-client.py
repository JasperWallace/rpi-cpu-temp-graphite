#!/usr/bin/env python
#
#
#

import sys, time, subprocess, argparse, graphiteclient
from rpitemp import Temp

parser = argparse.ArgumentParser(description='send rpi cpu temp stats to graphite.')

parser.add_argument('--server', type=str,
  help='The graphite server hostname')

parser.add_argument('--port',
  type=int,
  help='The graphite server port (defaults to 2003)')

bus = 0
parser.add_argument('--bus',
  type=int,
  help='The i2c bus to use')

addr = 0x52
parser.add_argument('--address',
  type=str,
  help='The device address on the i2c bus')

parser.add_argument('--lm75', action='store_true',
  help='Also measure an attached lm75 sensor')

args = parser.parse_args()

dolm75 = False
dolm75 = args.lm75

server = "bogwoppit.lan.pointless.net"
port = 2003

if args.server:
  server = args.server

if args.port:
  port = args.port

addr = 0x49
if args.address:
  if args.address.startswith("0x"):
    addr = int(args.address, 16)
  else:
    addr = int(args.address)

bus = 1
if args.bus is not None:
  bus = args.bus

temp = Temp()

def lm75temp(bus, addr):
#./tmp  /dev/i2c-0 0x49
  try:
    reading = subprocess.check_output(["./tmp", "/dev/i2c-%d" % (bus), "0x%02X" % (addr)], stderr=subprocess.STDOUT)
    reading = int(reading)
    return reading
  except Exception as err:
    print("getting temp failed with:")
    print(err)
#    print "output: ", err.output
#    print "return code: ", err.returncode
    return None

delay = 60

client = graphiteclient.GraphiteClient(server, port, delay)
client.verbose = True

while True:
  # Report temperatures
  client.poke("rpitemp.%s.cpu", int(temp.read() * 1000) )
  if dolm75:
    lmtemp = lm75temp(bus, addr)
    if lmtemp:
      client.poke("rpitemp.%s.lm75", lmtemp)

  sys.stdout.flush()
  time.sleep(delay)
